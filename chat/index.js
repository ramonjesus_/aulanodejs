const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const os = require('os');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const getLocalIpAddress = () => {
  const interfaces = os.networkInterfaces();
  for (const key in interfaces) {
    for (const iface of interfaces[key]) {
      if (!iface.internal && iface.family === 'IPv4') {
        return iface.address;
      }
    }
  }
  return '127.0.0.1'; // Retorno para localhost
};

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

const rooms = {
  chatGeral: 'Chat Geral',
  grupos: 'Grupos',
  comunidades: 'Comunidades',
  anuncios: 'Anúncios',
  vendas: 'Vendas',
  // Adicione mais salas conforme necessário
};

io.on('connection', (socket) => {
  console.log('Usuário conectado');

  socket.on('join_room', (room) => {
    if (rooms[room]) {
      socket.join(room);
      io.to(room).emit('chat_message', `Usuário ${socket.username} entrou na sala.`, room);
    }
  });

  socket.on('disconnect', () => {
    if (socket.username) {
      console.log(`Usuário ${socket.username} desconectado`);
      Object.keys(rooms).forEach((room) => {
        socket.leave(room);
      });
    } else {
      console.log('Usuário desconectado sem nome de usuário.');
    }
  });

  socket.on('chat_message', (msg, room) => {
    console.log(`Mensagem recebida de ${socket.username} na sala ${room}: ${msg}`);
    io.to(room).emit('chat_message', `${socket.username}: ${msg}`, room);
  });

  socket.on('set_username', (username) => {
    console.log(`Usuário ${username} conectado`);
    socket.username = username;
  });

  socket.on('disconnect_user', (usernameToDisconnect) => {
    const socketToDisconnect = Array.from(io.sockets.sockets.values()).find(
      (clientSocket) => clientSocket.username === usernameToDisconnect
    );

    if (socketToDisconnect) {
      socketToDisconnect.disconnect();
      console.log(`Usuário ${usernameToDisconnect} desconectado pelo administrador.`);
    } else {
      console.log(`Usuário ${usernameToDisconnect} não encontrado.`);
    }

    // Atualiza a lista de usuários para todos os clientes
    updateUserList();
  });

  const updateUserList = () => {
    const users = [];
    io.sockets.sockets.forEach((clientSocket) => {
      if (clientSocket.username) {
        users.push({
          username: clientSocket.username,
          ipAddress: clientSocket.request.connection.remoteAddress,
        });
      }
    });
    io.emit('user_list', users);
  };
});

server.listen(6000, () => {
  const ipAddress = getLocalIpAddress();
  console.log(`Servidor rodando em http://${ipAddress}:6000/`);
});
